
import './App.css';
import React, {Component} from "react";

class App extends Component{
    state = {
        initialArray: [],
        index: 2,
        generators: [],
        allGCDs: []
    }

    render() {
        return (
            <React.Fragment>
                <div className="App">
                    <h1>Vyhľadávač generátorov a podgrúp</h1>
                    <h3>Matej Peluha 97899</h3>
                    <div className="grupoid">
                        <h2>( Z</h2>
                        <input id="index-input" type="number" min="2" defaultValue="2"/>
                        <h2>, + )</h2>
                        <div id="button-container">
                            <button onClick={this.calculate}>Vyrátaj</button>
                        </div>
                    </div>
                    <div id="results">
                        <div id="subgroup-info">
                            <h3>Podgrupy:</h3>
                            <div id="subgroups-container">
                                {this.state.allGCDs.map(this.getSubgroup)}
                            </div>
                        </div>
                        <p id="generators"><p id="generators-h">Generátory: </p>{this.getGenerators()}</p>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    calculate = () =>{
        this.setInitialArray()
            .then(this.calculateResults);
    }

    async setInitialArray(){
        const index = this.getIndex();
        const initialArray = [];
        for(let i = 0; i < index; i++){
            initialArray.push(i);
        }

        const newState = {
            initialArray: initialArray,
            index: index,
            generators: this.state.generators,
            allGCDs: this.state.allGCDs
        }

        this.setState(newState)
    }

    getIndex(){
        let index = document.getElementById("index-input").value;
        if(index < 2){
            index = 2;
            document.getElementById("index-input").value = 2;
        }

        return index;
    }

    calculateResults = () =>{
        const generators = [];
        const allGCDs = [];

        for(let number of this.state.initialArray){
            if(number === 0){
                allGCDs.push(0);
                continue;
            }

            const gcd = this.getGCD(number, this.state.index);

            if(gcd === 1){
                generators.push(number);
            }

            if(!allGCDs.includes(gcd)) {
                allGCDs.push(gcd);
            }
        }

        const newState = {
            initialArray: this.state.initialArray,
            index: this.state.index,
            generators: generators,
            allGCDs: allGCDs
        }

        this.setState(newState, () => console.log(this.state));
    }

    getGCD(a, b) {
        if (!b) {
            return a;
        }

        return this.getGCD(b, a % b);
    }

    getGenerators() {
        return this.state.generators.toString();
    }

    getSubgroup = (gcd, key) => {
        const newIndex = this.state.index / gcd;

        if(gcd === 0){
            return (
                <div className="subgroup" key={key}>
                    ∅
                </div>
            );
        }

        return (
          <div className="subgroup" key={key}>
              <span>
                  (<span className="constant">{gcd}</span>Z<span className="index">{newIndex}</span>, +)
              </span>
          </div>
        );
    }
}

export default App;
